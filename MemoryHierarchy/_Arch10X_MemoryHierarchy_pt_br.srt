0
00:00:00,000 --> 00:00:03,040
tudo bem, nós precisamos aprender agora sobre

1
00:00:01,199 --> 00:00:05,040
os registradores do computador em geral, porque 

2
00:00:03,040 --> 00:00:06,799
você não conseguirá entender sobre assembly a menos que

3
00:00:05,040 --> 00:00:08,320
entenda sobre registradores nos quais 

4
00:00:06,799 --> 00:00:10,880
as linguagens assembly

5
00:00:08,320 --> 00:00:13,040
operam, então espero que em algum momento

6
00:00:10,880 --> 00:00:14,480
você tenha visto uma hierárquia de memória,

7
00:00:13,040 --> 00:00:15,360
mas se você não viu, vou falar 

8
00:00:14,480 --> 00:00:17,199
brevemente

9
00:00:15,360 --> 00:00:20,160
o ponto básico é que no topo da

10
00:00:17,199 --> 00:00:23,760
hierarquia estão os conteúdos que possuem

11
00:00:20,160 --> 00:00:26,480
um tamanho bem pequeno e de acesso muito rápido

12
00:00:23,760 --> 00:00:28,560
e também frequentemente são voláteis

13
00:00:26,480 --> 00:00:30,320
então haverá alguma memória volátil no

14
00:00:28,560 --> 00:00:30,800
topo e, em seguida, haverá uma memória de longo prazo

15
00:00:30,320 --> 00:00:33,200
armazenamentos

16
00:00:30,800 --> 00:00:34,480
de longo prazo continuam armazenando o conteúdo mesmo após

17
00:00:33,200 --> 00:00:36,960
o desligamento 

18
00:00:34,480 --> 00:00:37,920
e estão mais para baixo, os registradores

19
00:00:36,960 --> 00:00:40,079
estão bem no topo

20
00:00:37,920 --> 00:00:41,200
porque são a menor quantidade absoluta

21
00:00:40,079 --> 00:00:43,520
de memória

22
00:00:41,200 --> 00:00:45,360
frequentemente na ordem de dezenas de byte

23
00:00:43,520 --> 00:00:47,600
ou centenas de bytes

24
00:00:45,360 --> 00:00:48,960
e são muito rápidos de operar, 

25
00:00:47,600 --> 00:00:52,079
é por isso que o assembly

26
00:00:48,960 --> 00:00:53,760
os acessa diretamente, mas

27
00:00:52,079 --> 00:00:55,600
os registradores também são voláteis, portanto, quando você

28
00:00:53,760 --> 00:00:57,360
desligar o sistema, todo o conteúdo nos registradores

29
00:00:55,600 --> 00:00:58,960
serão perdidos

30
00:00:57,360 --> 00:01:01,280
descendo um pouco mais na hierarquia, você

31
00:00:58,960 --> 00:01:03,760
tem coisas como o cache do processador

32
00:01:01,280 --> 00:01:05,600
que é muito rápido e que é

33
00:01:03,760 --> 00:01:08,880
caro, para você saber,

34
00:01:05,600 --> 00:01:11,760
gerar e colocar dentro do processador

35
00:01:08,880 --> 00:01:13,280
e tem um tamanho pequeno, mas ainda vai 

36
00:01:11,760 --> 00:01:15,200
continuar perdendo o

37
00:01:13,280 --> 00:01:16,880
conteúdo da memória depois de

38
00:01:15,200 --> 00:01:17,280
desligar o sistema, é claro que a cache 

39
00:01:16,880 --> 00:01:19,119
tem objetivo 

40
00:01:17,280 --> 00:01:20,320
de fazer com que as coisas sejam mais rápidas

41
00:01:19,119 --> 00:01:23,920
passando para 

42
00:01:20,320 --> 00:01:26,400
a memória RAM de acesso aleátorio completo, então a memória RAM

43
00:01:23,920 --> 00:01:27,840
 é maior que o a cache, então a cache

44
00:01:26,400 --> 00:01:30,320
normalmente será da ordem de 

45
00:01:27,840 --> 00:01:32,000
megabytes, por exemplo, enquanto a RAM está

46
00:01:30,320 --> 00:01:32,720
na ordem dos gigabytes e a ram é bastante

47
00:01:32,000 --> 00:01:36,320
rápida, mas

48
00:01:32,720 --> 00:01:37,759
a cache é mais rápida, novamente todas essas 

49
00:01:36,320 --> 00:01:39,360
primeiras três memórias na hirerquia são

50
00:01:37,759 --> 00:01:41,280
memórias voláteis e que você perderá o

51
00:01:39,360 --> 00:01:42,399
conteúdo quando desligar o

52
00:01:41,280 --> 00:01:44,399
sistema

53
00:01:42,399 --> 00:01:45,439
a memória cache é bastante cara para construir dentro

54
00:01:44,399 --> 00:01:48,960
do processador 

55
00:01:45,439 --> 00:01:51,040
a memória ram é mais barata e possui um tamanho maior

56
00:01:48,960 --> 00:01:52,880
Ai vamos então entrar na parte dos armazenamentos

57
00:01:51,040 --> 00:01:55,920
não voláteis, como memórias flash ou

58
00:01:52,880 --> 00:01:58,640
memórias USB que conseguem reter o 

59
00:01:55,920 --> 00:02:00,479
conteúdo armazenado mesmo após o sistema ser desligado

60
00:01:58,640 --> 00:02:02,240
e como você sabe, normalmente essas memórias já armazenam dados 

61
00:02:00,479 --> 00:02:03,600
na ordem de gigabytes ou,

62
00:02:02,240 --> 00:02:06,079
eventualmente, já estamos começando a

63
00:02:03,600 --> 00:02:08,640
ter flash de terabytes

64
00:02:06,079 --> 00:02:09,759
então você tem os discos rígidos 

65
00:02:08,640 --> 00:02:11,360
mais lentos e baratos

66
00:02:09,759 --> 00:02:12,879
que possuem

67
00:02:11,360 --> 00:02:15,120
vários terabytes

68
00:02:12,879 --> 00:02:16,640
em seguinda, temos as unidades de fita que podem

69
00:02:15,120 --> 00:02:18,400
chegar nos petabytes

70
00:02:16,640 --> 00:02:19,920
então, o ponto com o qual vamos nos preocupar

71
00:02:18,400 --> 00:02:21,520
aqui, entretanto, são os

72
00:02:19,920 --> 00:02:23,840
registradores dentro do 

73
00:02:21,520 --> 00:02:23,840
computador

