0
00:00:00,560 --> 00:00:04,560
Olá pessoal, antes de começar essa

1
00:00:02,960 --> 00:00:06,480
grande aventura, talvez você queira

2
00:00:04,560 --> 00:00:08,080
saber por que nós, da OST, achamos que você deveria

3
00:00:06,480 --> 00:00:09,920
aprender assembly.

4
00:00:08,080 --> 00:00:12,000
Bom, vamos começar com uma fácil, a primeira coisa é que 

5
00:00:09,920 --> 00:00:13,840
diferente de algumas habilidades,

6
00:00:12,000 --> 00:00:15,440
como conhecimento em segurança de redes,

7
00:00:13,840 --> 00:00:16,960
o conhecimento em linguagem assembly é

8
00:00:15,440 --> 00:00:19,920
relativamente raro.

9
00:00:16,960 --> 00:00:22,160
Isso quer dizer que, quando as empresas realmente

10
00:00:19,920 --> 00:00:24,320
precisam de alguém que tenha essas habilidades

11
00:00:22,160 --> 00:00:26,560
a oferta e a demanda determina que o baixo

12
00:00:24,320 --> 00:00:30,240
fornecimento torna a habilidade

13
00:00:26,560 --> 00:00:31,840
mais valiosa. Outra razão é porque

14
00:00:30,240 --> 00:00:33,440
assembly é essencial para aprender

15
00:00:31,840 --> 00:00:35,120
engenharia reversa,

16
00:00:33,440 --> 00:00:37,040
seja fazendo engenharia reversa

17
00:00:35,120 --> 00:00:37,840
de software malicioso, como analista de

18
00:00:37,040 --> 00:00:40,160
malware,

19
00:00:37,840 --> 00:00:40,879
ou de um software benigno, como caçador de

20
00:00:40,160 --> 00:00:42,480
vulnerabilidades.

21
00:00:40,879 --> 00:00:44,239
Em ambos os casos, é 

22
00:00:42,480 --> 00:00:46,719
necessário saber disso,

23
00:00:44,239 --> 00:00:48,399
mas o problema é que mesmo se você fizer engenharia 

24
00:00:46,719 --> 00:00:50,079
reversa e encontrar alguma vulnerabilidade no

25
00:00:48,399 --> 00:00:51,840
software, você necessariamente não será capaz

26
00:00:50,079 --> 00:00:54,079
de explorar essas vulnerabilidades,

27
00:00:51,840 --> 00:00:55,840
a menos que você conheça assembly, porque encontrar

28
00:00:54,079 --> 00:00:58,640
vulnerabilidades é uma coisa

29
00:00:55,840 --> 00:01:00,239
e explorá-las é outra,

30
00:00:58,640 --> 00:01:01,440
talvez você queira ajudar a tornar os sistemas

31
00:01:00,239 --> 00:01:03,359
mais seguros

32
00:01:01,440 --> 00:01:05,199
o conhecimento de assembly é essencial para ir

33
00:01:03,359 --> 00:01:06,720
a fundo nas entranhas do sistemas

34
00:01:05,199 --> 00:01:08,320
para entender como ele funciona

35
00:01:06,720 --> 00:01:10,880
e fazê-lo 

36
00:01:08,320 --> 00:01:12,799
funcionar melhor e de forma mais segura.

37
00:01:10,880 --> 00:01:15,439
Hoje em dia, definitivamente 

38
00:01:12,799 --> 00:01:17,200
novos mecanismos de segurança estão profundamente ligados

39
00:01:15,439 --> 00:01:19,119
entre hardware e software

40
00:01:17,200 --> 00:01:20,960
e esses frequentemente assumem a forma

41
00:01:19,119 --> 00:01:22,799
de novas instruções de assembly

42
00:01:20,960 --> 00:01:25,040
que devem ser adotadas pelos autores

43
00:01:22,799 --> 00:01:27,520
dos compiladores nos sistemas operacionais ou

44
00:01:25,040 --> 00:01:30,240
firmware.

45
00:01:27,520 --> 00:01:32,079
Então, pessoalmente, eu frequentemente penso nas

46
00:01:30,240 --> 00:01:33,840
diferentes especializações e

47
00:01:32,079 --> 00:01:36,720
classes que os engenheiros de

48
00:01:33,840 --> 00:01:38,479
segurança podem ter

49
00:01:36,720 --> 00:01:39,520
como se fossem classes de um jogo de

50
00:01:38,479 --> 00:01:41,520
RPG,

51
00:01:39,520 --> 00:01:43,600
e eu fui e tentei desenterrar as 

52
00:01:41,520 --> 00:01:45,360
várias classes de um jogo de RPG como 

53
00:01:43,600 --> 00:01:46,079
Final Fantasy - os magos de várias

54
00:01:45,360 --> 00:01:47,680
cores -

55
00:01:46,079 --> 00:01:49,200
e eu descobri que os magos em particular

56
00:01:47,680 --> 00:01:50,880
estão espalhados em vários jogos diferentes de 

57
00:01:49,200 --> 00:01:52,079
videogame e, eu não consegui encontrar nenhuma 

58
00:01:50,880 --> 00:01:53,920
consistência entre eles.

59
00:01:52,079 --> 00:01:55,920
Então, em vez disso, você terá que me perdoar se 

60
00:01:53,920 --> 00:01:58,079
eu falar de forma geral 

61
00:01:55,920 --> 00:01:59,840
sobre cores mágicas enquanto

62
00:01:58,079 --> 00:02:02,000
falo sobre essas classes em particular.

63
00:01:59,840 --> 00:02:04,320
Por exemplo, você pode ser um mago

64
00:02:02,000 --> 00:02:06,880
branco - um arquiteto de segurança que está usando

65
00:02:04,320 --> 00:02:09,440
seus poderes de cura para

66
00:02:06,880 --> 00:02:10,879
proteger e curar sistemas quando

67
00:02:09,440 --> 00:02:13,200
são atacados.

68
00:02:10,879 --> 00:02:14,319
Você pode ser algo como um mago azul

69
00:02:13,200 --> 00:02:17,200
- usando poderes de

70
00:02:14,319 --> 00:02:18,800
espelhamento ou replicação para fazer

71
00:02:17,200 --> 00:02:20,800
engenharia reversa de software,

72
00:02:18,800 --> 00:02:22,000
aprendendo sobre o software tão

73
00:02:20,800 --> 00:02:25,520
bem, se não melhor,

74
00:02:22,000 --> 00:02:26,800
do que os engenheiros originais. Você pode ser um

75
00:02:25,520 --> 00:02:29,680
mago verde

76
00:02:26,800 --> 00:02:30,560
- como analistas de malware que são

77
00:02:29,680 --> 00:02:33,280
frequentemente

78
00:02:30,560 --> 00:02:36,319
e altamente envovildos com o bestiário que são

79
00:02:33,280 --> 00:02:38,720
os software maliciosos presentes na internet,

80
00:02:36,319 --> 00:02:40,239
ou você pode, por exemplo, ser um mago vermelho - 

81
00:02:38,720 --> 00:02:42,080
com o poder agressivo 

82
00:02:40,239 --> 00:02:44,239
que persegue por 

83
00:02:42,080 --> 00:02:47,040
vulnerabilidade e tenta

84
00:02:44,239 --> 00:02:47,840
atacar o software. Finalmente, nós temos

85
00:02:47,040 --> 00:02:50,800
um exemplo

86
00:02:47,840 --> 00:02:52,480
de uma mistura adequada do mago negro do Final

87
00:02:50,800 --> 00:02:53,440
Fantasy e o mago negro de "Magic The Gathering"

88
00:02:52,480 --> 00:02:55,040
reunidos -

89
00:02:53,440 --> 00:02:56,720
invocando frequentemente os poderes da

90
00:02:55,040 --> 00:02:58,319
necromancia,

91
00:02:56,720 --> 00:03:00,879
atacando e matando o software

92
00:02:58,319 --> 00:03:03,840
e trazendo-o de volta para

93
00:03:00,879 --> 00:03:04,400
fazê-lo fazer o que eles querem que faça. Então,

94
00:03:03,840 --> 00:03:06,319
quero dizer,

95
00:03:04,400 --> 00:03:07,920
quando eu vejo as coisas dessa maneira, eu acho

96
00:03:06,319 --> 00:03:09,519
muito maneiro

97
00:03:07,920 --> 00:03:11,040
os diferentes tipos de habilidades que você

98
00:03:09,519 --> 00:03:13,040
pode aprender quando conhece a linguagem

99
00:03:11,040 --> 00:03:14,480
assembly.

100
00:03:13,040 --> 00:03:16,480
Agora, outra razão pela qual você pode precisar

101
00:03:14,480 --> 00:03:19,120
saber sobre assembly é porque muito frequentemente os 

102
00:03:16,480 --> 00:03:21,280
principais pesquisadores utilizam e trazem assembly

103
00:03:19,120 --> 00:03:23,200
para suas apresentações ou artigos,

104
00:03:21,280 --> 00:03:25,040
geralmente quando estão tentando mostrar algum

105
00:03:23,200 --> 00:03:25,680
ponto como exatamente o sistema se

106
00:03:25,040 --> 00:03:27,120
comporta,

107
00:03:25,680 --> 00:03:28,879
a fim de provar a você que,

108
00:03:27,120 --> 00:03:30,159
por exemplo, a vulnerabilidade estava lá 

109
00:03:28,879 --> 00:03:32,080
ou algo do tipo

110
00:03:30,159 --> 00:03:33,680
e assim, para que você consiga compreender, você

111
00:03:32,080 --> 00:03:34,640
precisa ser capaz de falar a mesma

112
00:03:33,680 --> 00:03:36,000
linguagem que eles.

113
00:03:34,640 --> 00:03:38,080
Você precisa entender o que eles estão

114
00:03:36,000 --> 00:03:39,040
tentando dizer a você. Dei uma olhada rápida

115
00:03:38,080 --> 00:03:41,200
em algumas

116
00:03:39,040 --> 00:03:42,959
palestras dos útimos anos de uma

117
00:03:41,200 --> 00:03:45,840
conferência em particular

118
00:03:42,959 --> 00:03:47,760
e encontrei alguns exemplos disso. Agora, aqui, 

119
00:03:45,840 --> 00:03:50,000
meu nepotismo está aparecendo porque

120
00:03:47,760 --> 00:03:51,840
esta é a palestra da minha esposa do ano

121
00:03:50,000 --> 00:03:53,360
passado, utilizando assembly ARM

122
00:03:51,840 --> 00:03:54,879
para mostrar uma vulnerabilidade

123
00:03:53,360 --> 00:03:55,760
específica de integer underflow 

124
00:03:54,879 --> 00:03:57,360
em que

125
00:03:55,760 --> 00:03:59,760
 ela encontrou na ROM de um 

126
00:03:57,360 --> 00:04:01,599
chip bluetooth e como ela era

127
00:03:59,760 --> 00:04:04,319
explorável.

128
00:04:01,599 --> 00:04:05,040
Aqui tem um exemplo de assembly x86 de

129
00:04:04,319 --> 00:04:06,799
32 bits,

130
00:04:05,040 --> 00:04:08,799
quando alguém estava fazendo um retrospectiva

131
00:04:06,799 --> 00:04:10,480
dos vários mecanismos da cadeia de

132
00:04:08,799 --> 00:04:12,560
exploração e como eles poderiam ser utilizados

133
00:04:10,480 --> 00:04:14,640
para a vulnerabilidade do stuxnet

134
00:04:12,560 --> 00:04:17,680
de vários anos atrás.

135
00:04:14,640 --> 00:04:20,239
Aqui temos um exemplo de assembly intel de 64 bits

136
00:04:17,680 --> 00:04:22,320
sobre como investigar a pilha de drivers de wi-fi

137
00:04:20,239 --> 00:04:25,280
da apple.

138
00:04:22,320 --> 00:04:26,639
Aqui tem um assembly ARM e Intel 

139
00:04:25,280 --> 00:04:29,040
quando alguém estava falando sobre o

140
00:04:26,639 --> 00:04:30,479
mecanismo que o Windows utiliza para traduzir

141
00:04:29,040 --> 00:04:34,960
os binários Intel

142
00:04:30,479 --> 00:04:36,880
para executáveis ARM em tempo real.

143
00:04:34,960 --> 00:04:39,440
E então você pode ter algo

144
00:04:36,880 --> 00:04:43,040
assim: o assembly MIPS utilizado para atacar

145
00:04:39,440 --> 00:04:43,440
um roteador wi-fi. Frequentemente você

146
00:04:43,040 --> 00:04:46,160
terá

147
00:04:43,440 --> 00:04:47,040
pesquisadores utilizando coisas mais obscuras

148
00:04:46,160 --> 00:04:49,199
como esse exemplo: 

149
00:04:47,040 --> 00:04:50,320
uma linguagem assembly customizada, raramente 

150
00:04:49,199 --> 00:04:53,840
utilizada, mas que

151
00:04:50,320 --> 00:04:56,320
foi encontrada dentro de uma camada física de controle

152
00:04:53,840 --> 00:04:57,360
de acesso a um dispositivo wi-fi da

153
00:04:56,320 --> 00:04:59,280
broadcom

154
00:04:57,360 --> 00:05:01,120
utilizado para receber e processar

155
00:04:59,280 --> 00:05:02,800
pacotes.

156
00:05:01,120 --> 00:05:05,520
Ou isso, por exemplo, é um assembly ARC

157
00:05:02,800 --> 00:05:06,560
utilizando no controlador incorporado em dispositivos

158
00:05:05,520 --> 00:05:08,720
lenovo 

159
00:05:06,560 --> 00:05:11,600
que possui recursos e funcionalidades de segurança

160
00:05:08,720 --> 00:05:13,520
adicionais nos laptops da lenovo.

161
00:05:11,600 --> 00:05:15,360
Esses são alguns exemplos rápidos 

162
00:05:13,520 --> 00:05:16,960
de como pesquisadores tem

163
00:05:15,360 --> 00:05:19,280
utilizado assembly para mostrar

164
00:05:16,960 --> 00:05:21,280
seu ponto de vista em várias palestras.

165
00:05:19,280 --> 00:05:22,880
E finalmente, se nada disso 

166
00:05:21,280 --> 00:05:24,960
convenceu você de por quê você aprender

167
00:05:22,880 --> 00:05:27,280
assembly, espero que isso conveça:

168
00:05:24,960 --> 00:05:29,120
para ser um hacker você

169
00:05:27,280 --> 00:05:31,120
deve ser curioso,

170
00:05:29,120 --> 00:05:32,800
deve querer entender como os

171
00:05:31,120 --> 00:05:35,199
sistemas funcionam e o por quê,

172
00:05:32,800 --> 00:05:36,560
e frequentemente a linguagem assembly é

173
00:05:35,199 --> 00:05:39,759
essencial para entender

174
00:05:36,560 --> 00:05:41,600
exatamente como o software está se comportando e porque,

175
00:05:39,759 --> 00:05:43,280
e no final das contas entender

176
00:05:41,600 --> 00:05:44,720
como o sistemas funciona, quando você é uma pessoa muito 

177
00:05:43,280 --> 00:05:49,360
curiosa,

178
00:05:44,720 --> 00:05:49,360
é extremamente satisfatório.
