1
00:00:00,280 --> 00:00:04,680
I'd like to briefly introduce you to the

2
00:00:02,200 --> 00:00:06,279
concept of endianness this is something

3
00:00:04,680 --> 00:00:09,000
you don't have to care about when you're

4
00:00:06,279 --> 00:00:11,040
dealing with normal C source code but

5
00:00:09,000 --> 00:00:13,040
once you start digging below C and deal

6
00:00:11,040 --> 00:00:15,360
with assembly it quickly becomes

7
00:00:13,040 --> 00:00:18,160
important so first of all the term

8
00:00:15,360 --> 00:00:21,199
endianness came from Jonathan Swift's

9
00:00:18,160 --> 00:00:23,599
Gulliver's Travels novel and in that

10
00:00:21,199 --> 00:00:26,679
novel which was satirical it talked

11
00:00:23,599 --> 00:00:29,279
about two peoples who went to war over

12
00:00:26,679 --> 00:00:30,720
the correct way to eat a softboiled egg

13
00:00:29,279 --> 00:00:33,600
whether it should be with the little

14
00:00:30,720 --> 00:00:35,960
endian the little end first or the big

15
00:00:33,600 --> 00:00:38,360
end first so basically which way do you

16
00:00:35,960 --> 00:00:39,879
flip it and so it was satirically

17
00:00:38,360 --> 00:00:41,600
talking about you know the stupid

18
00:00:39,879 --> 00:00:43,760
reasons that England and France had gone

19
00:00:41,600 --> 00:00:47,280
to war over the years in computer

20
00:00:43,760 --> 00:00:50,280
architecture endianness refers to which

21
00:00:47,280 --> 00:00:53,320
byte of a multi-byte storage value will be

22
00:00:50,280 --> 00:00:55,760
stored into the lower memory address so

23
00:00:53,320 --> 00:00:58,960
in a little endian architecture the

24
00:00:55,760 --> 00:01:02,640
least significant byte LSB or the little

25
00:00:58,960 --> 00:01:05,239
end of the multi-byte value is stored into

26
00:01:02,640 --> 00:01:06,799
the lowest address so if we had some

27
00:01:05,239 --> 00:01:11,240
value that we would normally write in

28
00:01:06,799 --> 00:01:15,040
hexadecimal like hex 1 2 3 4 5 6 7 8 the

29
00:01:11,240 --> 00:01:17,560
78 is the least significant BTE of this

30
00:01:15,040 --> 00:01:20,640
4 byte value that I'm showing you here

31
00:01:17,560 --> 00:01:24,119
consequently if the 78 was stored into a

32
00:01:20,640 --> 00:01:27,320
memory address you would find the 56 at

33
00:01:24,119 --> 00:01:30,400
1 byte higher memory address and the 34

34
00:01:27,320 --> 00:01:32,479
at 1 byte further and the 12 at one byte

35
00:01:30,400 --> 00:01:34,079
further so the least significant byte

36
00:01:32,479 --> 00:01:36,200
goes into the lowest address and the

37
00:01:34,079 --> 00:01:38,920
most significant byte goes into a higher

38
00:01:36,200 --> 00:01:40,960
address now intel architectures are the

39
00:01:38,920 --> 00:01:43,119
classic example of things that have

40
00:01:40,960 --> 00:01:45,520
always been little endian on the other

41
00:01:43,119 --> 00:01:48,079
hand big endian is when you take the

42
00:01:45,520 --> 00:01:50,880
most significant byte or the big end of

43
00:01:48,079 --> 00:01:53,600
a multi-byte value and you store it that

44
00:01:50,880 --> 00:01:57,920
at the lowest address so for instance if

45
00:01:53,600 --> 00:02:00,640
we had again 1 2 3 4 5 6 7 8 hex1 two is

46
00:01:57,920 --> 00:02:03,240
the most significant byte in in a big

47
00:02:00,640 --> 00:02:06,280
endian architecture you would store the

48
00:02:03,240 --> 00:02:08,720
one two at the lowest address so one two

49
00:02:06,280 --> 00:02:12,040
would go at some address 34 we go at the

50
00:02:08,720 --> 00:02:14,120
next address one Higher 56 one Higher 78

51
00:02:12,040 --> 00:02:16,120
one Higher if you ever look at Network

52
00:02:14,120 --> 00:02:18,760
packet data you will find out that

53
00:02:16,120 --> 00:02:21,000
actually that is sent in big endian form

54
00:02:18,760 --> 00:02:23,160
so basically the big end of multi-byte

55
00:02:21,000 --> 00:02:25,920
values is going to be stored at the

56
00:02:23,160 --> 00:02:28,319
lowest address and you can do something

57
00:02:25,920 --> 00:02:30,360
like man byte order on a posix

58
00:02:28,319 --> 00:02:32,560
compatible system to find all the

59
00:02:30,360 --> 00:02:35,360
various Network to host reordering

60
00:02:32,560 --> 00:02:37,440
functions because basically if you're on

61
00:02:35,360 --> 00:02:39,200
a little endian architecture like Intel

62
00:02:37,440 --> 00:02:41,560
for instance and if the network traffic

63
00:02:39,200 --> 00:02:43,519
is coming in big end then you need to

64
00:02:41,560 --> 00:02:45,120
flip around to become little endian

65
00:02:43,519 --> 00:02:47,640
before the network traffic is stored

66
00:02:45,120 --> 00:02:50,480
into memory now many RISC-based systems

67
00:02:47,640 --> 00:02:53,000
like power PC spark myips Etc they

68
00:02:50,480 --> 00:02:55,000
started as big endian architectures but

69
00:02:53,000 --> 00:02:57,720
now they can be configured as either big

70
00:02:55,000 --> 00:03:00,280
endian or little endian and so that is

71
00:02:57,720 --> 00:03:03,080
referred to as bi-andian so I want to be

72
00:03:00,280 --> 00:03:05,000
clear that endianness applies to memory not

73
00:03:03,080 --> 00:03:06,799
registers when you're looking at

74
00:03:05,000 --> 00:03:08,280
registers and there's a multi-byte

75
00:03:06,799 --> 00:03:10,599
register if the register is bigger than

76
00:03:08,280 --> 00:03:13,000
one byte then you are always going to

77
00:03:10,599 --> 00:03:15,280
see the value presented in a typical

78
00:03:13,000 --> 00:03:18,319
mathematical notation of most

79
00:03:15,280 --> 00:03:20,560
significant values to the left and least

80
00:03:18,319 --> 00:03:23,239
significant values to the right so

81
00:03:20,560 --> 00:03:25,640
endianness only applies to how multi-byte

82
00:03:23,239 --> 00:03:28,159
values are stored in memory and again I

83
00:03:25,640 --> 00:03:30,840
said multi-byte values this is multi-byte

84
00:03:28,159 --> 00:03:33,159
storage so this only app to bytes not

85
00:03:30,840 --> 00:03:35,040
bits one of the things that people can

86
00:03:33,159 --> 00:03:37,000
get confused about early on when they

87
00:03:35,040 --> 00:03:39,000
first find out about it is that you know

88
00:03:37,000 --> 00:03:41,239
we can have single byte values like a

89
00:03:39,000 --> 00:03:43,319
character in C and this doesn't apply to

90
00:03:41,239 --> 00:03:44,640
that at all because it's not multi byte

91
00:03:43,319 --> 00:03:46,840
but then you start thinking about

92
00:03:44,640 --> 00:03:48,720
multi-byte values you see a two byte value

93
00:03:46,840 --> 00:03:50,680
a four byte value and it's stored into

94
00:03:48,720 --> 00:03:52,760
memory with the least significant byte

95
00:03:50,680 --> 00:03:54,519
at a lower address and then the student

96
00:03:52,760 --> 00:03:56,400
might think like okay wait am I supposed

97
00:03:54,519 --> 00:03:57,840
to you know I see this stuff in memory

98
00:03:56,400 --> 00:04:00,360
and I know I'm supposed to like flip

99
00:03:57,840 --> 00:04:02,560
around the bytes but am I supposed to

100
00:04:00,360 --> 00:04:05,560
flip around the individual bits and the

101
00:04:02,560 --> 00:04:07,319
answer is no this only pertains to bytes

102
00:04:05,560 --> 00:04:10,319
so you should always think of it as if

103
00:04:07,319 --> 00:04:12,000
your most significant bits are on the

104
00:04:10,319 --> 00:04:13,680
left and your least significant bits are

105
00:04:12,000 --> 00:04:15,680
on the right unless you're reading some

106
00:04:13,680 --> 00:04:18,440
documentation for some sort of manual

107
00:04:15,680 --> 00:04:20,239
that decides to draw it otherwise you

108
00:04:18,440 --> 00:04:22,639
know just the general mathematical

109
00:04:20,239 --> 00:04:25,000
notation of you know binary logic and

110
00:04:22,639 --> 00:04:27,240
stuff like that this is the you know

111
00:04:25,000 --> 00:04:28,800
ones column this is the twoos columns

112
00:04:27,240 --> 00:04:30,759
this is The Four's columns this is the

113
00:04:28,800 --> 00:04:33,039
Eight's column in the same way that when

114
00:04:30,759 --> 00:04:35,440
we have decimal we have the ones the

115
00:04:33,039 --> 00:04:36,759
tens the 100s Etc so the least

116
00:04:35,440 --> 00:04:38,479
significant bits are on the right the

117
00:04:36,759 --> 00:04:39,479
most significant bits are on the left

118
00:04:38,479 --> 00:04:42,000
and when you're thinking about

119
00:04:39,479 --> 00:04:44,320
endianness don't be tempted to flip

120
00:04:42,000 --> 00:04:47,000
around the bit ordering because

121
00:04:44,320 --> 00:04:49,160
endianness only pertains to bytes not

122
00:04:47,000 --> 00:04:51,639
bits so just to try to visualize that

123
00:04:49,160 --> 00:04:54,160
for you let's imagine we have a 4 byte

124
00:04:51,639 --> 00:04:56,360
register and it holds the hexadecimal value

125
00:04:54,160 --> 00:04:58,120
feed face and so we could have a little

126
00:04:56,360 --> 00:05:00,639
endian architecture and a big endian

127
00:04:58,120 --> 00:05:02,880
architecture and I'm going to draw low

128
00:05:00,639 --> 00:05:04,320
addresses low and high addresses high as

129
00:05:02,880 --> 00:05:06,400
is my style and which you'll see

130
00:05:04,320 --> 00:05:08,560
throughout the rest of this class so a

131
00:05:06,400 --> 00:05:10,600
low address this is literally memory

132
00:05:08,560 --> 00:05:12,759
address zero and memory address one and

133
00:05:10,600 --> 00:05:15,720
memory address 2 if the hexadecimal

134
00:05:12,759 --> 00:05:17,840
value feed face was stored to memory at

135
00:05:15,720 --> 00:05:21,199
address zero on a big endian

136
00:05:17,840 --> 00:05:23,600
architecture the big end the Fe would be

137
00:05:21,199 --> 00:05:25,199
stored at the low address the next most

138
00:05:23,600 --> 00:05:28,280
significant byte would be stored at the

139
00:05:25,199 --> 00:05:30,280
next address and so forth in contrast on

140
00:05:28,280 --> 00:05:33,080
a little endian architecture

141
00:05:30,280 --> 00:05:35,520
if you writing feed face from a register

142
00:05:33,080 --> 00:05:38,199
that's 4 bytes wide into memory at

143
00:05:35,520 --> 00:05:40,319
address zero it is the least significant

144
00:05:38,199 --> 00:05:42,759
bit the least significant byte the

145
00:05:40,319 --> 00:05:45,960
little end which will be stored into

146
00:05:42,759 --> 00:05:48,639
memory at the lower address so CE goes in

147
00:05:45,960 --> 00:05:50,319
here FA goes in here and so forth so

148
00:05:48,639 --> 00:05:53,520
what this means in practice when you're

149
00:05:50,319 --> 00:05:55,560
looking at tools like debuggers is that

150
00:05:53,520 --> 00:05:58,000
if you choose to display something in

151
00:05:55,560 --> 00:06:02,680
bytes you will see literally you know

152
00:05:58,000 --> 00:06:08,240
this E7 is at this address 7fs 920 and

153
00:06:02,680 --> 00:06:12,720
this be is at 7fs 921 922 923 924 925

154
00:06:08,240 --> 00:06:15,240
926 927 928 so basically each byte

155
00:06:12,720 --> 00:06:18,160
that's displayed is going to be at a

156
00:06:15,240 --> 00:06:19,599
increasing address moving from left to

157
00:06:18,160 --> 00:06:21,919
right but if you know that the

158
00:06:19,599 --> 00:06:23,840
underlying C source code is using

159
00:06:21,919 --> 00:06:26,520
something like a short which is 2 byes

160
00:06:23,840 --> 00:06:28,960
wide when you read this memory display

161
00:06:26,520 --> 00:06:30,919
you need to take endianness into account

162
00:06:28,960 --> 00:06:33,800
are if something reads two bytes is it

163
00:06:30,919 --> 00:06:35,560
going to read the value E7 be not if

164
00:06:33,800 --> 00:06:38,919
it's little endian if it's little endian

165
00:06:35,560 --> 00:06:41,400
it's going to read the value be E7 and

166
00:06:38,919 --> 00:06:43,479
if you ask a debugger to display values

167
00:06:41,400 --> 00:06:45,199
as multi-byte values then it will take

168
00:06:43,479 --> 00:06:47,919
that into account and it'll display it

169
00:06:45,199 --> 00:06:50,360
in the typical notation of most

170
00:06:47,919 --> 00:06:52,479
significant values most significant

171
00:06:50,360 --> 00:06:55,199
digits to the left right if we're in

172
00:06:52,479 --> 00:06:58,039
hexadecimal this is the ones the 16s column

173
00:06:55,199 --> 00:06:59,960
the 256s and so forth so most

174
00:06:58,039 --> 00:07:01,919
significant digits to the left least

175
00:06:59,960 --> 00:07:04,000
significant digits to the right so again

176
00:07:01,919 --> 00:07:06,360
if you ask the debugger to display

177
00:07:04,000 --> 00:07:09,800
things here I'm saying display 16

178
00:07:06,360 --> 00:07:11,639
hexadecimal halfword where GTB calls a half

179
00:07:09,800 --> 00:07:14,160
word two bytes at a time you can see

180
00:07:11,639 --> 00:07:17,680
that it flipped the order of these the

181
00:07:14,160 --> 00:07:19,199
value that was at 920 is E7 and that is

182
00:07:17,680 --> 00:07:21,319
the little end because this is a little

183
00:07:19,199 --> 00:07:24,319
endian architecture that this debugger

184
00:07:21,319 --> 00:07:26,800
was run on same thing if I do eight

185
00:07:24,319 --> 00:07:28,800
hexadecimal words or four byte values

186
00:07:26,800 --> 00:07:30,840
you can see once again the values have

187
00:07:28,800 --> 00:07:34,280
been flipped around so we don't just

188
00:07:30,840 --> 00:07:38,960
read this across the row like this e7b

189
00:07:34,280 --> 00:07:40,759
3713 no we read it 1 337b E7 so

190
00:07:38,960 --> 00:07:42,960
fundamentally as you learn the assembly

191
00:07:40,759 --> 00:07:45,000
instructions they will always be telling

192
00:07:42,960 --> 00:07:46,240
you whether they are operating what size

193
00:07:45,000 --> 00:07:48,800
of data they're operating on whether

194
00:07:46,240 --> 00:07:51,080
it's 2 bytes four bytes 8 bytes and so

195
00:07:48,800 --> 00:07:53,400
forth but if you go in with a debugger

196
00:07:51,080 --> 00:07:55,960
all of a sudden endianness matters and

197
00:07:53,400 --> 00:07:57,680
you need to be looking at memory in the

198
00:07:55,960 --> 00:07:59,360
same granularity as the assembly

199
00:07:57,680 --> 00:08:01,159
instructions are operating on memory

200
00:07:59,360 --> 00:08:03,879
whether they're you know reading in four

201
00:08:01,159 --> 00:08:05,680
bytes reading in 8 bytes because if you

202
00:08:03,879 --> 00:08:07,840
just look at it with some smaller

203
00:08:05,680 --> 00:08:09,000
granularity and you look across the row

204
00:08:07,840 --> 00:08:11,120
and you're on a little endian

205
00:08:09,000 --> 00:08:13,159
architecture then that is not going to

206
00:08:11,120 --> 00:08:14,720
be an accurate representation of the

207
00:08:13,159 --> 00:08:17,120
values that are actually being operated

208
00:08:14,720 --> 00:08:17,120
on

